CREATE TABLE app_user 
( 
 user_id INTEGER CONSTRAINT pk_user_id PRIMARY KEY, 
 user_name VARCHAR(40) CONSTRAINT user_not_null NOT NULL 
);

INSERT INTO app_user (user_id, user_name) VALUES (1, 'Pavel');
INSERT INTO app_user (user_id, user_name) VALUES (2, 'Petr');
INSERT INTO app_user (user_id, user_name) VALUES (3, 'Jan');
INSERT INTO app_user (user_id, user_name) VALUES (4, 'Jiri');