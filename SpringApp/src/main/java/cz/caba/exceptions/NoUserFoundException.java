/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.caba.exceptions;

/**
 *
 * @author micha
 */
public class NoUserFoundException extends Exception {
    /**
     * Parameterless constructor.
     */
    public NoUserFoundException() {
        super();
    }

    /**
     * Constructor which creates exception with custom message.
     *
     * @param message for exception.
     */
    public NoUserFoundException(String message) {
        super(message);
    }

    /**
     * Constructor which creates exception with custom cause.
     *
     * @param cause of exception.
     */
    public NoUserFoundException(Throwable cause) {
        super(cause);
    }

    /**
     *  Constructor which creates exception with custom message and cause.
     *
     * @param message for exception.
     * @param cause of exception.
     */
    public NoUserFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
