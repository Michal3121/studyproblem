package cz.caba.dao;

import cz.caba.entity.User;
import cz.caba.exceptions.NoUserFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
@Transactional
public class UserDao {
    
    // An EntityManager will be automatically injected from entityManagerFactory
    // setup on DatabaseConfig class.
    @PersistenceContext
    private EntityManager entityManager;
    
    /**
    * Return the user having the passed id.
    */
    public User getUserById(int id) throws NoUserFoundException {
        User userToRet = entityManager.find(User.class, id);
        if(userToRet == null) throw new NoUserFoundException();
        
        return userToRet;
    }
    
}