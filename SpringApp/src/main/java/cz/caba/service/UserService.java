package cz.caba.service;

import cz.caba.dao.UserDao;
import cz.caba.entity.User;
import cz.caba.exceptions.NoUserFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.bind.annotation.ResponseBody;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @ResponseBody
    public User getUserById(int id) throws NoUserFoundException {
        return this.userDao.getUserById(id);
    }
    
}