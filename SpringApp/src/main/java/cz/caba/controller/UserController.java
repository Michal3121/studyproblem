package cz.caba.controller;

import cz.caba.entity.User;
import cz.caba.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;
  
    @RequestMapping(value={"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<?> getUserById(@PathVariable("id") int id) {
        User userToRet; 
        try {
            userToRet = this.userService.getUserById(id);
        } catch(Exception ex) {
            String errorMessage = "User not found: " + ex.toString();
            
            return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
        }
        
        return new ResponseEntity<>(userToRet, HttpStatus.OK);
    }
    
}