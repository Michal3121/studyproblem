var data = [
  {id: 1, name: "Peter"},
  {id: 2, name: "Oldo"},
  {id: 3, name: "Robert"}
];


class SimpleTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: [], text: '', rowToEditDialog:'', showEditDialog: false};

    this.handleTableRowRemove = this.handleTableRowRemove.bind(this);
    this.handleTableRowEdit = this.handleTableRowEdit.bind(this);
    this.handleEditDialog = this.handleEditDialog.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleTableRowAdd = this.handleTableRowAdd.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  handleTableRowRemove(rowID) {
    var index = -1;  
    var datalength = this.state.data.length;
    for( var i = 0; i < datalength; i++ ) {
      if( this.state.data[i].id === rowID ) {
        index = i;
        break;
      }
    }
    this.state.data.splice( index, 1 );  
    this.setState( {data: this.state.data} );
  }

  handleTableRowEdit(rowID) {
    var index = -1;  
    var datalength = this.state.data.length;
    for( var i = 0; i < datalength; i++ ) {
      if( this.state.data[i].id === rowID ) {
        index = i;
        break;
      }
    }
    var selectRowToEditDialog = this.state.data[index];
    this.setState({rowToEditDialog: selectRowToEditDialog});
    this.setState({showEditDialog: true});
  }

  handleEditDialog(editName) {
    var index = -1;
    var datalength = this.state.data.length;
    for( var i = 0; i < datalength; i++ ) {
      if( this.state.data[i].id === this.state.rowToEditDialog.id ) {
        index = i;
        break;
      }
    } 
    this.state.data[index].name = editName;
    this.setState({showEditDialog: false});
  }

  handleTextChange(e) {
    this.setState({text: e.target.value});
  }

  handleTableRowAdd() {
    var datalength = this.state.data.length;
    if(datalength == 0) {
      var lastElementID = 0;
    }
    else {
      var lastElementID = this.state.data[datalength-1].id;
    }
    lastElementID++; 

    var newUser = [{id: lastElementID, name:this.state.text}];    
    this.setState( {data: this.state.data.concat(newUser)} );
    this.setState({text: ''});
  }

  componentDidMount() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  }

  render() {
    return (
      <div className="simpleTable">
        <table>
        <TableHead />
        <TableBody 
          data={this.state.data} 
          onRowRemove={this.handleTableRowRemove} 
          onRowEdit={this.handleTableRowEdit}
          disabledRowButtons={this.state.showEditDialog} 
        />
        </table>
        <input 
          type="text" 
          placeholder="Meno" 
          value={this.state.text}
          onChange={this.handleTextChange}
          disabled={this.state.showEditDialog}
        />
        <input 
          type="submit" 
          value="Pridat" 
          onClick={this.handleTableRowAdd} 
          disabled={this.state.showEditDialog} 
        />
        {this.state.showEditDialog ? <EditRowDialog OKClick={this.handleEditDialog} nameToEdit={this.state.rowToEditDialog.name} /> : null}
      </div>
    );
  }
}

var EditRowDialog = React.createClass({
  getInitialState: function() {
    return {
      editName: this.props.nameToEdit
    };
  },

  handleOKEvent: function() {
    this.props.OKClick(this.state.editName);
  },

  handleNameChange: function(e) {
    this.setState({editName: e.target.value});
  },

  render: function() {
    return (
      <div className="okDialog">
        <input 
          type="text"  
          value={this.state.editName}
          onChange={this.handleNameChange}
        />
        <input type="submit" value="OK" onClick={this.handleOKEvent} />
      </div>
    );
  }
});

var TableHead = React.createClass({
  render: function() {
    return (
      <thead className="tableHead">
        <tr>
          <th>ID</th>
          <th>Name</th> 
        </tr>
      </thead>
    );
  }
});

var TableBody = React.createClass({
  handleRowRemoveEvent: function(rowID){
    this.props.onRowRemove(rowID);
  },
  handleRowEditEvent: function(rowID){
    this.props.onRowEdit(rowID);
  },
  render: function() {
    var tableRows = this.props.data.map(function(row) {
      return (
        <TableRow 
          id={row.id} 
          name={row.name} 
          key={row.id} 
          onRemove={this.handleRowRemoveEvent}
          onEdit={this.handleRowEditEvent}
          disabledButtons={this.props.disabledRowButtons}
        />
      );
    },this);
    return (
      <tbody className="tableBody">
        {tableRows}
      </tbody>
    );
  }
});

var TableRow = React.createClass({
  handleRemove: function() {
    this.props.onRemove(this.props.id);
    return false;
  },
  handleEdit: function() {
    this.props.onEdit(this.props.id);
    return false;
  },
  render: function() {
    return (
      <tr className="tableRow">
        <TableCell cellContent={this.props.id} />
        <TableCell cellContent={this.props.name} />
        <td>
          <input 
            type="submit" 
            value="Smazat" 
            onClick={this.handleRemove} 
            disabled={this.props.disabledButtons} 
          />
        </td>
        <td>
          <input 
            type="submit" 
            value="Upravit" 
            onClick={this.handleEdit} 
            disabled={this.props.disabledButtons} 
          />
        </td>
      </tr>
    );
  }
});

var TableCell = React.createClass({
  render: function() {
    return (
      <td className="tableCell">
        {this.props.cellContent}
      </td>
    );
  }
});

ReactDOM.render(
  <SimpleTable url="users.json" />,
  document.getElementById('content')
);